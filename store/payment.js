import { make } from 'vuex-pathify'
import { PaymentService } from '../services'

export const state = () => ({
})

export const actions = {
  async createPaymentSlip ({ rootState: { auth: { token } } }, body) {
    try {
      await PaymentService.postPaymentSlip(body, token)
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

export const mutations = make.mutations(state)
