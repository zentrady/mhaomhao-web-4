import { make } from 'vuex-pathify'
import { EmploymentService } from '../services'

const mapData = value => value.results
const mapPage = (value) => {
  return {
    count: value.count,
    next: value.next,
    previous: value.previous
  }
}

export const state = () => ({
  employments: [],
  employmentsPage: {},
  employment: {},
  myEmployment: {}
})

export const actions = {
  async fetchEmployments ({ rootState: { auth: { token } }, commit }, params) {
    try {
      const result = await EmploymentService.getEmployments(params, token)
      console.log(result)
      commit('SET_EMPLOYMENTS', mapData(result))
      commit('SET_EMPLOYMENTS_PAGE', mapPage(result))
    } catch (error) {
      return Promise.reject(error)
    }
  },
  async fetchEmployment ({ rootState: { auth: { token } }, commit }, id) {
    try {
      const result = await EmploymentService.getEmployment(id, token)
      console.log(result)
      commit('SET_EMPLOYMENT', result)
    } catch (error) {
      return Promise.reject(error)
    }
  },

  async createEmployment ({ rootState: { auth: { token } } }, body) {
    try {
      const result = await EmploymentService.createEmployment(body, token)
      return Promise.resolve(result)
    } catch (error) {
      return Promise.reject(error)
    }
  },

  async fetchMyEmployment ({ rootState: { auth: { token } }, commit }, id) {
    try {
      const result = await EmploymentService.getMyEmployment(token)
      console.log(result)
      commit('SET_MY_EMPLOYMENT', result)
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

export const mutations = make.mutations(state)
